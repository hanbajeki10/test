<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\Posts;
use App\Http\Requests\PostsRequest;
use Validator;
use Exception;
use DB;
use App\Http\Resources\PostsResource;
   
class PostsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::all();
    
        // dd($posts);
        return $this->sendResponse(PostsResource::collection($posts), 'posts retrieved successfully.');
    }
    /**
     * Store a newly created resource in stoproductrage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {

        try {
            Posts::create($request->validated());
            return new PostsResource(true, "Store Posts Success", $request->all());
        } catch (Exception $e) {
            return new PostsResource(false, "Store posts Failed", $e);
        }
    }
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $posts = Posts::find($id);
            return new PostsResource(true, 'Posts  Found',$posts);
        } catch (Exception $e) {
            return new PostsResource(false, 'Posts Failed ' . $e);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try{
        $posts = Posts::find($id)->update($request->all());
        return new PostsResource(true, "Posts Update", $posts);
       } catch (Exception $e){
        return new PostsResource(false, "Posts Update Failed" . $e);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $posts = Posts::find($id);
            if ($posts) {
                $posts->delete();
                return new PostsResource(true, "Posts Deleted");
            }
            return new PostsResource(true, "Posts Not Found");
        } catch (Exception $e) {
            return new PostsResource(true, "Posts Delete Failed", $e);
        }
    }
}