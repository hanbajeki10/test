-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: localhost    Database: article
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2022_09_22_051420_create_posts_table',1),(2,'2014_10_12_000000_create_users_table',2),(3,'2014_10_12_100000_create_password_resets_table',2),(4,'2019_08_19_000000_create_failed_jobs_table',2),(5,'2019_12_14_000001_create_personal_access_tokens_table',2),(6,'2022_09_22_051420_create_article_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
INSERT INTO `personal_access_tokens` VALUES (1,'App\\Models\\User',1,'MyApp','69e4dc9d314d736d561e5e6a5d829ef1a0a4d48618f0900a6b33dc76cb86db7d','[\"*\"]',NULL,NULL,'2022-09-22 19:25:43','2022-09-22 19:25:43'),(2,'App\\Models\\User',1,'MyApp','ee29776009763eb89a977436934633217c624fcd555482e2688545ca5f31049f','[\"*\"]',NULL,NULL,'2022-09-22 19:28:01','2022-09-22 19:28:01'),(3,'App\\Models\\User',1,'MyApp','76df606f0fcc5917f543c65cf9cec48de4768fbcccea664a44d24eabae8e681c','[\"*\"]',NULL,NULL,'2022-09-22 19:52:43','2022-09-22 19:52:43'),(4,'App\\Models\\User',3,'MyApp','c6734320b4ebef9bfb47be6de05bf30f8aa443e6e1b99273263db0ecf64b8376','[\"*\"]',NULL,NULL,'2022-09-22 19:54:20','2022-09-22 19:54:20'),(5,'App\\Models\\User',3,'MyApp','3d1f52c2d592ae9e901566142a7189861186a5f4ab48187a75a3246134de508d','[\"*\"]',NULL,NULL,'2022-09-22 19:54:24','2022-09-22 19:54:24'),(6,'App\\Models\\User',3,'MyApp','5325b69610ae247adce8a828b39c8fe8697affbc5e0a64cb2d79185a26b38ae3','[\"*\"]',NULL,NULL,'2022-09-22 20:16:46','2022-09-22 20:16:46'),(7,'App\\Models\\User',3,'MyApp','4bc89f09250a54aba93fee964271a16d71d918da19672fd6b0b0319e9a4217da','[\"*\"]',NULL,NULL,'2022-09-22 20:22:19','2022-09-22 20:22:19'),(8,'App\\Models\\User',3,'MyApp','9fb88569ece66016902c8f9ab4f573c748c2688dc80d55e860574b7aab1508be','[\"*\"]',NULL,NULL,'2022-09-22 20:25:36','2022-09-22 20:25:36'),(9,'App\\Models\\User',3,'MyApp','a2b2f1f43ca3974f8ccc18fad0e66413d4aa79875b63d7ffc14a77186786982d','[\"*\"]',NULL,NULL,'2022-09-22 20:28:53','2022-09-22 20:28:53'),(10,'App\\Models\\User',3,'MyApp','c7c2c7f102c4f1632d2c26ce896a7923813126e8ba1ad22a7a84bcd64f15eadc','[\"*\"]',NULL,NULL,'2022-09-22 20:40:43','2022-09-22 20:40:43'),(11,'App\\Models\\User',3,'MyApp','501d8622d87224b59001ac1eec198ca541813482ef258569446cdeb58e0c20ac','[\"*\"]',NULL,NULL,'2022-09-22 20:41:52','2022-09-22 20:41:52'),(12,'App\\Models\\User',5,'MyApp','5ed4fee1ee41c06ba7797a5fb7c1ac9a281a63d78da9ad9c2be3ca9a20fa8a5f','[\"*\"]',NULL,NULL,'2022-09-22 20:59:36','2022-09-22 20:59:36'),(13,'App\\Models\\User',3,'MyApp','b0c12dab3296084eb624f0b04cae651e6406363ef5e72efc5d7dc7365a3bdeb4','[\"*\"]',NULL,NULL,'2022-09-22 20:59:53','2022-09-22 20:59:53'),(14,'App\\Models\\User',5,'MyApp','20c81701078054928d31ce679b1611912ea83aafeebdeef0791c9034cf101ec1','[\"*\"]',NULL,NULL,'2022-09-22 21:00:00','2022-09-22 21:00:00'),(15,'App\\Models\\User',5,'MyApp','ab2c2f3368d80c0f2c4fba9fa951d70b403792b14274287c0882931394790253','[\"*\"]',NULL,NULL,'2022-09-22 22:32:43','2022-09-22 22:32:43'),(16,'App\\Models\\User',5,'MyApp','6506b41d8f0a6abbc07d57b38550b208dcbee1f941b172b2eb78c9e90ed5938f','[\"*\"]',NULL,NULL,'2022-09-22 22:33:52','2022-09-22 22:33:52'),(17,'App\\Models\\User',5,'MyApp','b1b3df1ecea2a9f1ceb5c92fd54b80ab2bb87c0b8e6ad985b0a1e7065d4b0363','[\"*\"]',NULL,NULL,'2022-09-22 22:43:49','2022-09-22 22:43:49'),(18,'App\\Models\\User',5,'MyApp','3b02516293e6de763762bcdd4035ed5f2fcb52c0085588ed6551a5eaa149913e','[\"*\"]',NULL,NULL,'2022-09-22 23:01:48','2022-09-22 23:01:48'),(19,'App\\Models\\User',5,'MyApp','6430f848b9ed661ea51b7c30a063ff1029fab191867e88c6e959c2d4afb05d6c','[\"*\"]',NULL,NULL,'2022-09-22 23:02:12','2022-09-22 23:02:12'),(20,'App\\Models\\User',5,'MyApp','b7eb81bcfbe7bebf61a4c78a4d2ef07054397fac109e00f8a9cd3792bcc6f740','[\"*\"]','2022-09-24 19:29:22',NULL,'2022-09-22 23:16:21','2022-09-24 19:29:22'),(21,'App\\Models\\User',5,'MyApp','ae9db4ee38503c456b3e130b9d5ab0e1937f53fbc20f6f03108f34f381f26e3c','[\"*\"]','2022-09-23 03:02:39',NULL,'2022-09-23 03:00:42','2022-09-23 03:02:39'),(22,'App\\Models\\User',6,'MyApp','ed8370d0a020ef36623c44ba6ea7038a57b601a8e00fa73875992a7228673e76','[\"*\"]','2022-09-23 03:02:13',NULL,'2022-09-23 03:01:37','2022-09-23 03:02:13');
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (2,'1','wer','1','aktif','2022-09-23 01:15:11','2022-09-23 01:15:11'),(3,'2','test developer','3','draft','2022-09-23 01:24:05','2022-09-24 18:44:35'),(4,'4','Full stack','3','draft','2022-09-23 03:04:17','2022-09-24 18:48:33'),(5,'1','wer','1','publish','2022-09-23 03:10:30','2022-09-23 03:10:30'),(6,'1','wer','1','publish','2022-09-23 03:17:21','2022-09-23 03:17:21'),(7,'1','wer','1','publish','2022-09-23 03:25:29','2022-09-23 03:25:29'),(8,'1','wer','1','publish','2022-09-23 03:27:25','2022-09-23 03:27:25'),(9,'1','wer','1','publish','2022-09-23 03:29:56','2022-09-23 03:29:56'),(10,'1','wer','1','publish','2022-09-23 03:32:47','2022-09-23 03:32:47'),(11,'123456789101112131415161718','qwertyuioplkjhgfdaszxcvbn','1e1','publish','2022-09-24 19:23:32','2022-09-24 19:23:32');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'hani','hani@gmail.com',NULL,'$2y$10$xCmDrcE103pvILobrGHlDuRrnSiJvLm4OQtHS2504iAaLIVncNHwO',NULL,'2022-09-22 19:25:43','2022-09-22 19:25:43'),(3,'hani','han@gmail.com',NULL,'$2y$10$4fqkGSIthxdZ2DNsZ16cj.hW7OtjWje51Lhi2MS3/lQPrLrjo8ymi',NULL,'2022-09-22 19:54:20','2022-09-22 19:54:20'),(5,'hani','hann@gmail.com',NULL,'$2y$10$IjKOu5XK49QZmxgFLlK/Re//Xusacq.C5kp5AN4Sqb4s8SQ3XqfLe',NULL,'2022-09-22 20:59:36','2022-09-22 20:59:36'),(6,'hani','han30@gmail.com',NULL,'$2y$10$UhINdGLuEHbLPBRm6egL4OXrk9jVMZmgLjG3MJLlxosXNbrTB4PI.',NULL,'2022-09-23 03:01:37','2022-09-23 03:01:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'article'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-30 11:31:05
